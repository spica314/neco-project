# Summary

- [Chapter 1](./chapter_1.md)
- [Chapter 2](./chapter_2.md)
    - [Phase 1](./chapter_2/phase_1.md)
    - [Phase 2](./chapter_2/phase_2.md)
    - [Phase 3](./chapter_2/phase_3.md)
    - [Phase 4](./chapter_2/phase_4.md)
    - [Phase 5](./chapter_2/phase_5.md)
    - [Phase 6](./chapter_2/phase_6.md)
    - [Phase 7](./chapter_2/phase_7.md)
    - [Phase 8](./chapter_2/phase_8.md)

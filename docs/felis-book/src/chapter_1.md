# Chapter 1

大きめのやりたいこと
- [ ] x86向け一通りコンパイル
- [ ] PTXコードを生成してGPUで ray tracing in a weekend
- [ ] OpenQASMを生成してなんかの量子計算
- [ ] なんかの高位合成
- [ ] Calculus of Inductive Construction かなんかで証明
- etc.

方針
- 括弧なし関数呼び出し
- 基本文法は最小限
- 型推論後のASTを使える?
- キーワードと名前かぶってもok?
- インタプリタ型実行あり
- 組み込みの関数はgenericしない?
- AST書き換え?によって情報が付加されるのはok, 情報が損失するのはng

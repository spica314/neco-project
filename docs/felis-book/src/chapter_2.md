# Chapter 2

未振り分け
- should_panic
- struct, .
- enum, (simple)match
- loop, continue, break
- mutable
- impl, =>
- trait, impl
- i32以外のinteger
- f32, f64, fnm
- array?
- string
- allocate
- use
- monad系のなんか
    - [mtl](https://hackage.haskell.org/package/mtl-2.2.2) あたりを参考に
- lambda

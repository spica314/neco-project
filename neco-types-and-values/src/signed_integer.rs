use std::ops::{Add, Mul};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct SignedInteger {
    bits: usize,
    value: i64,
}

impl SignedInteger {
    pub fn new(bits: usize) -> SignedInteger {
        SignedInteger { bits, value: 0 }
    }
    pub fn to_string(&self, radix: i64) -> String {
        assert!(radix == 10);
        format!("{}", self.value)
    }
    pub fn as_i32(&self) -> i32 {
        self.value as i32
    }
}

impl Add for &SignedInteger {
    type Output = SignedInteger;
    fn add(self, rhs: &SignedInteger) -> Self::Output {
        assert_eq!(self.bits, rhs.bits);
        SignedInteger {
            bits: self.bits,
            value: self.value + rhs.value,
        }
    }
}

impl Add<i64> for &SignedInteger {
    type Output = SignedInteger;
    fn add(self, rhs: i64) -> Self::Output {
        SignedInteger {
            bits: self.bits,
            value: self.value + rhs,
        }
    }
}

impl Mul for &SignedInteger {
    type Output = SignedInteger;
    fn mul(self, rhs: &SignedInteger) -> Self::Output {
        assert_eq!(self.bits, rhs.bits);
        SignedInteger {
            bits: self.bits,
            value: self.value * rhs.value,
        }
    }
}

impl Mul<i64> for &SignedInteger {
    type Output = SignedInteger;
    fn mul(self, rhs: i64) -> Self::Output {
        SignedInteger {
            bits: self.bits,
            value: self.value * rhs,
        }
    }
}

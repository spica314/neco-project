use std::collections::BTreeMap;

use crate::values::Value;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ValueMap {
    map: BTreeMap<String, Value>,
}

impl ValueMap {
    pub fn new(map: BTreeMap<String, Value>) -> ValueMap {
        ValueMap { map }
    }
    pub fn get(&self, name: &str) -> Option<&Value> {
        self.map.get(name)
    }
}

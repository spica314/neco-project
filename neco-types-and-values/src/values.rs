use crate::{signed_integer::*, value_map::ValueMap};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Value {
    Map(ValueMap),
    Signed(SignedInteger),
    Empty,
}

impl Value {
    pub fn as_i32(&self) -> Option<i32> {
        match self {
            Value::Signed(signed_integer) => {
                let res = signed_integer.as_i32();
                Some(res)
            }
            _ => None,
        }
    }
}

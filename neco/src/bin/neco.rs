use neco::{
    interpreter::Interpreter,
    syn::{lex, FelisFile, Parse},
};

struct Args {
    file_path: String,
    test: bool,
}

impl Args {
    pub fn parse() -> Args {
        let args: Vec<_> = std::env::args().collect();
        let mut file_path = None;
        let mut test = false;
        for arg in args.iter().skip(1) {
            if arg == "--test" {
                test = true;
                continue;
            }
            file_path = Some(arg.to_string());
        }
        Args {
            file_path: file_path.unwrap(),
            test,
        }
    }
}

fn main() {
    let args = Args::parse();

    let s = std::fs::read_to_string(&args.file_path).unwrap();
    let tokens = lex(&s);
    eprintln!("{:?}", tokens);
    let file = FelisFile::parse(&tokens, &mut 0).unwrap().unwrap();
    eprintln!("{:?}", file);
    let mut interpreter = Interpreter::new();
    interpreter.load(file);

    if args.test {
        interpreter.test();
    } else {
        interpreter.exec_main();
    }
}

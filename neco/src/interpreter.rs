use std::collections::BTreeMap;

use neco_types_and_values::{signed_integer::SignedInteger, value_map::ValueMap, values::Value};

use crate::syn::{
    def_fn::DefFn, definition::Definition, expr::Expr, expr_lit::ExprLit, statement::Statement,
    FelisFile,
};

pub struct Bindings {
    stack: Vec<Vec<(String, Value)>>,
}

impl Bindings {
    pub fn new() -> Bindings {
        Bindings {
            stack: vec![vec![]],
        }
    }
    pub fn enter_scope(&mut self) {
        self.stack.push(vec![])
    }
    pub fn leave_scope(&mut self) {
        self.stack.pop();
    }
    pub fn get(&self, name: &str) -> Option<&Value> {
        for xs in self.stack.iter().rev() {
            for (bind_name, value) in xs.iter().rev() {
                if bind_name == name {
                    eprintln!("binding.get {} -> {:?}", name, value);
                    return Some(value);
                }
            }
        }
        None
    }
    pub fn get_mut(&mut self, name: &str) -> Option<&mut Value> {
        for xs in self.stack.iter_mut().rev() {
            for (bind_name, value) in xs.iter_mut().rev() {
                if bind_name == name {
                    return Some(value);
                }
            }
        }
        None
    }
    pub fn bind(&mut self, name: &str, v: Value) {
        self.stack.last_mut().unwrap().push((name.to_string(), v));
    }
}

pub struct Environment {
    bindings: Bindings,
}

impl Environment {
    pub fn new() -> Environment {
        Environment {
            bindings: Bindings::new(),
        }
    }
    pub fn eval(&mut self, expr: &Expr) -> Value {
        match expr {
            Expr::Lit(lit) => match lit {
                ExprLit::Ident(ident) => {
                    let v = self.bindings.get(ident.as_str());
                    if let Some(v) = v {
                        v.clone()
                    } else {
                        panic!("unknown ident {:?}", ident)
                    }
                }
                ExprLit::Number(number) => {
                    let v = number.as_str().parse::<i64>().unwrap();
                    let mut res = SignedInteger::new(32);
                    res = &res + v;
                    Value::Signed(res)
                }
            },
            Expr::App(app) => {
                let mut evaluated_args = vec![];
                for arg in app.args() {
                    let t = self.eval(arg);
                    evaluated_args.push(t);
                }
                if app.f().to_string() == "__assert_eq_i32" {
                    assert!(evaluated_args.len() == 2);
                    if evaluated_args[0] != evaluated_args[1] {
                        panic!("assert failed: {:?} != {:?}", app.args()[0], app.args()[1]);
                    }
                    Value::Empty
                } else if app.f().to_string() == "__add_i32" {
                    assert!(evaluated_args.len() == 2);
                    let r =
                        evaluated_args[0].as_i32().unwrap() + evaluated_args[1].as_i32().unwrap();
                    let mut v = SignedInteger::new(32);
                    v = &v + (r as i64);
                    Value::Signed(v)
                } else {
                    panic!("f = {}", app.f().to_string());
                }
            }
            Expr::Member(expr_member) => {
                let v = self.eval(expr_member.parent());
                match v {
                    Value::Map(value_map) => value_map
                        .get(expr_member.member().as_str())
                        .unwrap()
                        .clone(),
                    _ => panic!(),
                }
            }
            Expr::Struct(expr_struct) => {
                let mut map = BTreeMap::new();
                for member in expr_struct.members() {
                    let v = self.eval(member.expr());
                    map.insert(member.ident().as_str().to_string(), v);
                }
                Value::Map(ValueMap::new(map))
            }
        }
    }
}

pub struct Interpreter {
    file: Option<FelisFile>,
}

impl Interpreter {
    pub fn new() -> Interpreter {
        Interpreter { file: None }
    }
    pub fn load(&mut self, file: FelisFile) {
        self.file = Some(file);
    }
    pub fn exec_main(&self) {
        let mut environment = Environment::new();
        for def in self.file.as_ref().unwrap().definitions() {
            match def {
                Definition::Fn(def_fn) => {
                    if def_fn.ident().to_string() == "main" {
                        let res = self.exec_fn(def_fn, &mut environment);
                        std::process::exit(res.map(|x| x.as_i32().unwrap()).unwrap_or(0));
                    }
                }
                _ => {}
            }
        }
    }
    pub fn test(&self) {
        for def in self.file.as_ref().unwrap().definitions() {
            match def {
                Definition::Fn(def_fn) => {
                    if def_fn
                        .attributes()
                        .iter()
                        .any(|x| x.ident().as_str() == "test")
                    {
                        let mut environment = Environment::new();
                        let res = self.exec_fn(def_fn, &mut environment);
                        eprintln!("res = {:?}", res);
                    }
                }
                _ => {}
            }
        }
    }
    pub fn exec_fn(&self, def_fn: &DefFn, environment: &mut Environment) -> Option<Value> {
        let block = def_fn.block();
        for statement in block.statements() {
            match statement {
                Statement::Let(let_statement) => {
                    let v = environment.eval(let_statement.expr());
                    environment.bindings.bind(let_statement.ident().as_str(), v);
                }
                Statement::Expr(expr_statement) => {
                    environment.eval(expr_statement.expr());
                }
            }
        }
        if let Some(expr) = block.expr() {
            let res = environment.eval(expr);
            Some(res)
        } else {
            None
        }
    }
}

pub mod attribute;
pub mod block;
pub mod def_fn;
pub mod def_struct;
pub mod def_struct_member;
pub mod definition;
pub mod expr;
pub mod expr_app;
pub mod expr_lit;
pub mod expr_member;
pub mod expr_struct;
pub mod expr_struct_member;
pub mod return_type;
pub mod statement;
pub mod statement_expr;
pub mod statement_let;
pub mod tokens;

use crate::syn::tokens::{
    token_assign::TokenAssign, token_dot::TokenDot, token_ident::TokenIdent,
    token_lbrace::TokenLBrace, token_number::TokenNumber, token_rbrace::TokenRBrace,
    token_semi::TokenSemi,
};

use self::{
    definition::Definition,
    tokens::{
        token_arrow::TokenArrow, token_camma::TokenCamma, token_colon::TokenColon,
        token_comment::TokenComment, token_lbracket::TokenLBracket, token_pound::TokenPound,
        token_rbracket::TokenRBracket,
    },
};

#[derive(Debug, Clone)]
pub enum ParseResult<T> {
    Ok(T),
    Fail,
}

impl<T> ParseResult<T> {
    pub fn is_fail(&self) -> bool {
        matches!(self, ParseResult::Fail)
    }
    pub fn unwrap(self) -> T {
        match self {
            ParseResult::Ok(t) => t,
            _ => panic!(),
        }
    }
    pub fn to_option(self) -> Option<T> {
        match self {
            ParseResult::Ok(x) => Some(x),
            ParseResult::Fail => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum ParseError {
    Error(FilePos),
}

pub trait Parse
where
    Self: Sized,
{
    type R;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError>;
}

fn parse_keyword(
    tokens: &[Token],
    i: &mut usize,
    s: &str,
) -> Result<ParseResult<Token>, ParseError> {
    if tokens[*i].to_string() == s {
        let res = tokens[*i].clone();
        *i += 1;
        Ok(ParseResult::Ok(res))
    } else {
        Ok(ParseResult::Fail)
    }
}

#[derive(Debug, Clone)]
pub struct FelisFile {
    definitions: Vec<Definition>,
}

impl FelisFile {
    pub fn definitions(&self) -> &[Definition] {
        &self.definitions
    }
}

impl Parse for FelisFile {
    type R = FelisFile;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        let mut definitions = vec![];

        loop {
            let def = Definition::parse(tokens, i)?;
            if let ParseResult::Ok(def) = def {
                definitions.push(def);
            } else {
                break;
            }
        }

        if *i != tokens.len() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }

        Ok(ParseResult::Ok(FelisFile { definitions }))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct FilePos {
    pub row: usize,
    pub col: usize,
}

#[derive(Debug, Clone)]
pub struct Token {
    begin_pos: FilePos,
    end_pos: FilePos,
    t: TokenInner,
}

impl Token {
    pub fn begin_pos(&self) -> FilePos {
        self.begin_pos
    }
    pub fn end_pos(&self) -> FilePos {
        self.end_pos
    }
    pub fn inner(&self) -> &TokenInner {
        &self.t
    }
    pub fn to_string(&self) -> String {
        self.t.to_string()
    }
    pub fn as_str(&self) -> &str {
        self.t.as_str()
    }
}

impl AsRef<TokenInner> for Token {
    fn as_ref(&self) -> &TokenInner {
        &self.t
    }
}

#[derive(Debug, Clone)]
pub enum TokenInner {
    Ident(TokenIdent),
    Number(TokenNumber),
    Arrow(TokenArrow),
    LBrace(TokenLBrace),
    RBrace(TokenRBrace),
    Comment(TokenComment),
    Assign(TokenAssign),
    Semi(TokenSemi),
    Pound(TokenPound),
    LBracket(TokenLBracket),
    RBracket(TokenRBracket),
    Colon(TokenColon),
    Camma(TokenCamma),
    Dot(TokenDot),
}

impl TokenInner {
    pub fn to_string(&self) -> String {
        self.as_str().to_string()
    }
    pub fn as_str(&self) -> &str {
        match self {
            TokenInner::Ident(ident) => ident.as_str(),
            TokenInner::Number(number) => number.as_str(),
            TokenInner::Arrow(_) => "->",
            TokenInner::LBrace(_) => "{",
            TokenInner::RBrace(_) => "}",
            TokenInner::Assign(_) => "=",
            TokenInner::Semi(_) => ";",
            TokenInner::Pound(_) => "#",
            TokenInner::LBracket(_) => "[",
            TokenInner::RBracket(_) => "]",
            TokenInner::Colon(_) => ":",
            TokenInner::Camma(_) => ",",
            TokenInner::Dot(_) => ".",
            TokenInner::Comment(_) => todo!(),
        }
    }
}

fn chars_with_file_pos(s: &str) -> Vec<(char, FilePos)> {
    let mut res = vec![];
    let mut row = 0;
    let mut col = 0;
    for c in s.chars() {
        let file_pos = FilePos { row, col };
        res.push((c, file_pos));
        if c == '\n' {
            row += 1;
            col = 0;
        } else {
            col += 1;
        }
    }
    res.push(('\0', FilePos { row, col }));
    res
}

fn matches_symbol(cs: &[(char, FilePos)], i: &mut usize, s: &str, t: TokenInner) -> Option<Token> {
    let s_cs: Vec<_> = s.chars().collect();
    for k in *i..*i + s_cs.len() {
        if cs[k].0 != s_cs[k - *i] {
            return None;
        }
    }
    let k = *i + s_cs.len();
    let token = Token {
        begin_pos: cs[*i].1,
        end_pos: cs[k].1,
        t,
    };
    *i = k;
    Some(token)
}

pub fn lex(s: &str) -> Vec<Token> {
    let mut res = vec![];
    let cs = chars_with_file_pos(s);
    let mut i = 0;
    while i < cs.len() {
        let (c, _) = cs[i];
        if c == '\0' {
            break;
        }
        if c.is_whitespace() {
            i += 1;
            continue;
        }
        if c.is_ascii_alphabetic() || c == '_' {
            let mut k = i;
            let mut s = String::new();
            while k < cs.len() {
                let (c, _) = cs[k];
                if c.is_ascii_alphanumeric() || c == '_' {
                    s.push(c);
                    k += 1;
                } else {
                    break;
                }
            }
            let token = Token {
                begin_pos: cs[i].1,
                end_pos: cs[k].1,
                t: TokenInner::Ident(TokenIdent::new(s)),
            };
            res.push(token);
            i = k;
            continue;
        }
        if c.is_ascii_digit() {
            let mut k = i;
            let mut s = String::new();
            while k < cs.len() {
                let (c, _) = cs[k];
                if c.is_ascii_alphanumeric() || c == '_' {
                    s.push(c);
                    k += 1;
                } else {
                    break;
                }
            }
            let token = Token {
                begin_pos: cs[i].1,
                end_pos: cs[k].1,
                t: TokenInner::Number(TokenNumber::new(s)),
            };
            res.push(token);
            i = k;
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "->", TokenInner::Arrow(TokenArrow)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "{", TokenInner::LBrace(TokenLBrace)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "}", TokenInner::RBrace(TokenRBrace)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "=", TokenInner::Assign(TokenAssign)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, ";", TokenInner::Semi(TokenSemi)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "#", TokenInner::Pound(TokenPound)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "[", TokenInner::LBracket(TokenLBracket)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, "]", TokenInner::RBracket(TokenRBracket)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, ":", TokenInner::Colon(TokenColon)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, ",", TokenInner::Camma(TokenCamma)) {
            res.push(token);
            continue;
        }
        if let Some(token) = matches_symbol(&cs, &mut i, ".", TokenInner::Dot(TokenDot)) {
            res.push(token);
            continue;
        }
        panic!("unknown character: {:?}", cs[i]);
    }
    res
}

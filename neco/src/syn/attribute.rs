use super::{
    tokens::{
        token_ident::TokenIdent, token_lbracket::TokenLBracket, token_pound::TokenPound,
        token_rbracket::TokenRBracket,
    },
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct Attribute {
    pound: Token,
    lbracket: Token,
    ident: Token,
    rbracket: Token,
}

impl Attribute {
    pub fn pound(&self) -> &Token {
        &self.pound
    }
    pub fn lbracket(&self) -> &Token {
        &self.lbracket
    }
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn rbracket(&self) -> &Token {
        &self.rbracket
    }
}

impl Parse for Attribute {
    type R = Attribute;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let pound = TokenPound::parse(tokens, i)?;
        if pound.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let pound = pound.unwrap();

        let lbracket = TokenLBracket::parse(tokens, i)?;
        if lbracket.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let lbracket = lbracket.unwrap();

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let ident = ident.unwrap();

        let rbracket = TokenRBracket::parse(tokens, i)?;
        if rbracket.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let rbracket = rbracket.unwrap();

        Ok(ParseResult::Ok(Attribute {
            pound,
            lbracket,
            ident,
            rbracket,
        }))
    }
}

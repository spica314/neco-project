use crate::syn::tokens::{token_lbrace::TokenLBrace, token_rbrace::TokenRBrace};

use super::{expr::Expr, statement::Statement, Parse, ParseError, ParseResult, Token};

#[derive(Debug, Clone)]
pub struct Block {
    lbrace: Token,
    statements: Vec<Statement>,
    expr: Option<Expr>,
    rbrace: Token,
}

impl Block {
    pub fn lbarce(&self) -> &Token {
        &self.lbrace
    }
    pub fn statements(&self) -> &[Statement] {
        &self.statements
    }
    pub fn expr(&self) -> Option<&Expr> {
        self.expr.as_ref()
    }
    pub fn rbrace(&self) -> &Token {
        &self.rbrace
    }
}

impl Parse for Block {
    type R = Block;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let lbrace = TokenLBrace::parse(tokens, i)?;
        if lbrace.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let lbrace = lbrace.unwrap();

        let mut statements = vec![];
        while let ParseResult::Ok(statement) = Statement::parse(tokens, i)? {
            statements.push(statement);
        }
        eprintln!("statements = {:?}", statements);

        let expr = Expr::parse(tokens, i)?;
        let expr = expr.to_option();
        eprintln!("expr = {:?}", expr);

        let rbrace = TokenRBrace::parse(tokens, i)?;
        if rbrace.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let rbrace = rbrace.unwrap();

        Ok(ParseResult::Ok(Block {
            lbrace,
            statements,
            expr,
            rbrace,
        }))
    }
}

use super::{
    attribute::Attribute, block::Block, parse_keyword, return_type::ReturnType,
    tokens::token_ident::TokenIdent, Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct DefFn {
    attributes: Vec<Attribute>,
    token_fn: Token,
    ident: Token,
    ret_ty: Option<ReturnType>,
    block: Block,
}

impl DefFn {
    pub fn attributes(&self) -> &[Attribute] {
        &self.attributes
    }
    pub fn token_fn(&self) -> &Token {
        &self.token_fn
    }
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn ret_ty(&self) -> Option<&ReturnType> {
        self.ret_ty.as_ref()
    }
    pub fn block(&self) -> &Block {
        &self.block
    }
}

impl Parse for DefFn {
    type R = DefFn;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let mut attributes = vec![];
        while let ParseResult::Ok(attribute) = Attribute::parse(tokens, i)? {
            attributes.push(attribute);
        }

        let token_fn = parse_keyword(tokens, i, "fn")?;
        if token_fn.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let token_fn = token_fn.unwrap();

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let ident = ident.unwrap();

        let ret_ty = ReturnType::parse(tokens, i)?;
        let ret_ty = ret_ty.to_option();

        let block = Block::parse(tokens, i)?;
        if block.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let block = block.unwrap();

        Ok(ParseResult::Ok(DefFn {
            attributes,
            token_fn,
            ident,
            ret_ty,
            block,
        }))
    }
}

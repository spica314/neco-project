use super::{
    attribute::Attribute,
    def_struct_member::DefStructMember,
    parse_keyword,
    tokens::{token_ident::TokenIdent, token_lbrace::TokenLBrace, token_rbrace::TokenRBrace},
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct DefStruct {
    attributes: Vec<Attribute>,
    token_struct: Token,
    ident: Token,
    lbrace: Token,
    members: Vec<DefStructMember>,
    rbrace: Token,
}

impl DefStruct {
    pub fn attributes(&self) -> &[Attribute] {
        &self.attributes
    }
    pub fn token_struct(&self) -> &Token {
        &self.token_struct
    }
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn lbrace(&self) -> &Token {
        &self.lbrace
    }
    pub fn members(&self) -> &[DefStructMember] {
        &self.members
    }
    pub fn rbrace(&self) -> &Token {
        &self.rbrace
    }
}

impl Parse for DefStruct {
    type R = DefStruct;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let mut attributes = vec![];
        while let ParseResult::Ok(attribute) = Attribute::parse(tokens, i)? {
            attributes.push(attribute);
        }

        let token_struct = parse_keyword(tokens, i, "struct")?;
        if token_struct.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let token_struct = token_struct.unwrap();

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let ident = ident.unwrap();

        let lbrace = TokenLBrace::parse(tokens, i)?;
        if lbrace.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let lbrace = lbrace.unwrap();

        let mut members = vec![];
        while let ParseResult::Ok(member) = DefStructMember::parse(tokens, i)? {
            members.push(member);
        }

        let rbrace = TokenRBrace::parse(tokens, i)?;
        if rbrace.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let rbrace = rbrace.unwrap();

        Ok(ParseResult::Ok(DefStruct {
            attributes,
            token_struct,
            ident,
            lbrace,
            members,
            rbrace,
        }))
    }
}

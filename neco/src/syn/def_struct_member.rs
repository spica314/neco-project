use super::{
    tokens::{token_camma::TokenCamma, token_colon::TokenColon, token_ident::TokenIdent},
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct DefStructMember {
    ident: Token,
    colon: Token,
    ty: Token,
    camma: Token,
}

impl DefStructMember {
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn colon(&self) -> &Token {
        &self.colon
    }
    pub fn ty(&self) -> &Token {
        &self.ty
    }
    pub fn camma(&self) -> &Token {
        &self.camma
    }
}

impl Parse for DefStructMember {
    type R = DefStructMember;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let ident = ident.unwrap();

        let colon = TokenColon::parse(tokens, i)?;
        if colon.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let colon = colon.unwrap();

        let ty = TokenIdent::parse(tokens, i)?;
        if ty.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let ty = ty.unwrap();

        let camma = TokenCamma::parse(tokens, i)?;
        if camma.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let camma = camma.unwrap();

        Ok(ParseResult::Ok(DefStructMember {
            ident,
            colon,
            ty,
            camma,
        }))
    }
}

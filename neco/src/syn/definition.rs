use super::{def_fn::DefFn, def_struct::DefStruct, Parse, ParseError, ParseResult, Token};

#[derive(Debug, Clone)]
pub enum Definition {
    Fn(DefFn),
    Struct(DefStruct),
}

impl Parse for Definition {
    type R = Definition;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let def_fn = DefFn::parse(tokens, i)?;
        if let ParseResult::Ok(def_fn) = def_fn {
            return Ok(ParseResult::Ok(Definition::Fn(def_fn)));
        }

        let def_struct = DefStruct::parse(tokens, i)?;
        if let ParseResult::Ok(def_struct) = def_struct {
            return Ok(ParseResult::Ok(Definition::Struct(def_struct)));
        }

        Ok(ParseResult::Fail)
    }
}

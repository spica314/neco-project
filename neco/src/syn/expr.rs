use super::{
    expr_app::ExprApp, expr_lit::ExprLit, expr_member::ExprMember, expr_struct::ExprStruct, Parse,
    ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub enum Expr {
    Lit(ExprLit),
    App(ExprApp),
    Struct(ExprStruct),
    Member(ExprMember),
}

impl Parse for Expr {
    type R = Expr;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        let initial_i = *i;
        eprintln!("start with {:?}", tokens[*i]);

        eprintln!("try struct construct");
        let expr_struct = ExprStruct::parse(tokens, i)?;
        if let ParseResult::Ok(expr_struct) = expr_struct {
            return Ok(ParseResult::Ok(Expr::Struct(expr_struct)));
        }

        eprintln!("try app");
        let app = ExprApp::parse(tokens, i)?;
        if let ParseResult::Ok(app) = app {
            return Ok(ParseResult::Ok(app));
        }

        eprintln!("debug: {:?}", tokens[*i]);
        eprintln!("try member");
        let expr_member = ExprMember::parse(tokens, i)?;
        if let ParseResult::Ok(expr_member) = expr_member {
            return Ok(ParseResult::Ok(expr_member));
        }

        eprintln!("fail expr");
        *i = initial_i;
        Ok(ParseResult::Fail)
    }
}

use super::{
    expr::Expr, expr_member::ExprMember, tokens::token_ident::TokenIdent, Parse, ParseError,
    ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct ExprApp {
    f: Token,
    args: Vec<Expr>,
}

impl ExprApp {
    pub fn f(&self) -> &Token {
        &self.f
    }
    pub fn args(&self) -> &[Expr] {
        &self.args
    }
}

impl Parse for ExprApp {
    type R = Expr;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let f = TokenIdent::parse(tokens, i)?;
        if f.is_fail() {
            return Ok(ParseResult::Fail);
        }
        let f = f.unwrap();

        let mut args = vec![];
        while let ParseResult::Ok(arg) = ExprMember::parse(tokens, i)? {
            args.push(arg);
        }
        if args.len() == 0 {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }

        Ok(ParseResult::Ok(Expr::App(ExprApp { f, args })))
    }
}

use super::{
    tokens::{token_ident::TokenIdent, token_number::TokenNumber},
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub enum ExprLit {
    Ident(Token),
    Number(Token),
}

impl Parse for ExprLit {
    type R = ExprLit;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        let token_number = TokenNumber::parse(tokens, i)?;
        if let ParseResult::Ok(token_number) = token_number {
            return Ok(ParseResult::Ok(ExprLit::Number(token_number)));
        }

        let token_ident = TokenIdent::parse(tokens, i)?;
        if let ParseResult::Ok(token_ident) = token_ident {
            return Ok(ParseResult::Ok(ExprLit::Ident(token_ident)));
        }

        Ok(ParseResult::Fail)
    }
}

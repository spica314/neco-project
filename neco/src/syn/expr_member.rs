use super::{
    expr::Expr,
    expr_lit::ExprLit,
    tokens::{token_dot::TokenDot, token_ident::TokenIdent},
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct ExprMember {
    parent: Box<Expr>,
    dot: Token,
    member: Token,
}

impl ExprMember {
    pub fn parent(&self) -> &Expr {
        self.parent.as_ref()
    }
    pub fn dot(&self) -> &Token {
        &self.dot
    }
    pub fn member(&self) -> &Token {
        &self.member
    }
}

impl Parse for ExprMember {
    type R = Expr;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let expr = ExprLit::parse(tokens, i)?;
        if expr.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let mut expr = Expr::Lit(expr.unwrap());

        if let ParseResult::Ok(dot) = TokenDot::parse(tokens, i)? {
            let member = TokenIdent::parse(tokens, i)?;
            if member.is_fail() {
                return Err(ParseError::Error(tokens[*i].begin_pos()));
            }
            let member = member.unwrap();

            expr = Expr::Member(ExprMember {
                parent: Box::new(expr),
                dot,
                member,
            });
        }

        Ok(ParseResult::Ok(expr))
    }
}

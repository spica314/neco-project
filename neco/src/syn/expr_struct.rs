use super::{
    expr_struct_member::ExprStructMember,
    tokens::{token_ident::TokenIdent, token_lbrace::TokenLBrace, token_rbrace::TokenRBrace},
    Parse, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct ExprStruct {
    ident: Token,
    lbrace: Token,
    members: Vec<ExprStructMember>,
    rbrace: Token,
}

impl ExprStruct {
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn lbrace(&self) -> &Token {
        &self.lbrace
    }
    pub fn members(&self) -> &[ExprStructMember] {
        &self.members
    }
    pub fn rbrace(&self) -> &Token {
        &self.rbrace
    }
}

impl Parse for ExprStruct {
    type R = ExprStruct;

    fn parse(
        tokens: &[Token],
        i: &mut usize,
    ) -> Result<super::ParseResult<Self::R>, super::ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let ident = ident.unwrap();

        let lbrace = TokenLBrace::parse(tokens, i)?;
        if lbrace.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let lbrace = lbrace.unwrap();

        let mut members = vec![];
        while let ParseResult::Ok(member) = ExprStructMember::parse(tokens, i)? {
            members.push(member);
        }

        let rbrace = TokenRBrace::parse(tokens, i)?;
        if rbrace.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let rbrace = rbrace.unwrap();

        Ok(ParseResult::Ok(ExprStruct {
            ident,
            lbrace,
            members,
            rbrace,
        }))
    }
}

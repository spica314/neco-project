use super::{
    expr::Expr,
    tokens::{token_camma::TokenCamma, token_colon::TokenColon, token_ident::TokenIdent},
    Parse, ParseError, ParseResult, Token,
};

#[derive(Debug, Clone)]
pub struct ExprStructMember {
    ident: Token,
    colon: Token,
    expr: Box<Expr>,
    camma: Token,
}

impl ExprStructMember {
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn colon(&self) -> &Token {
        &self.colon
    }
    pub fn expr(&self) -> &Expr {
        self.expr.as_ref()
    }
    pub fn camma(&self) -> &Token {
        &self.camma
    }
}

impl Parse for ExprStructMember {
    type R = ExprStructMember;

    fn parse(
        tokens: &[Token],
        i: &mut usize,
    ) -> Result<super::ParseResult<Self::R>, super::ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let ident = ident.unwrap();

        let colon = TokenColon::parse(tokens, i)?;
        if colon.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let colon = colon.unwrap();

        let expr = Expr::parse(tokens, i)?;
        if expr.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let expr = expr.unwrap();

        let camma = TokenCamma::parse(tokens, i)?;
        if camma.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let camma = camma.unwrap();

        Ok(ParseResult::Ok(ExprStructMember {
            ident,
            colon,
            expr: Box::new(expr),
            camma,
        }))
    }
}

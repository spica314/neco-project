use crate::syn::{parse_keyword, tokens::token_arrow::TokenArrow, ParseError, ParseResult};

use super::{Parse, Token};

#[derive(Debug, Clone)]
pub struct ReturnType {
    arrow: Token,
    ty: Token,
}

impl ReturnType {
    pub fn arrow(&self) -> &Token {
        &self.arrow
    }
    pub fn ty(&self) -> &Token {
        &self.ty
    }
}

impl Parse for ReturnType {
    type R = ReturnType;

    fn parse(
        tokens: &[super::Token],
        i: &mut usize,
    ) -> Result<super::ParseResult<Self::R>, super::ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let arrow = TokenArrow::parse(tokens, i)?;
        if arrow.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let arrow = arrow.unwrap();

        let ty = parse_keyword(tokens, i, "i32")?;
        if ty.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let ty = ty.unwrap();

        Ok(ParseResult::Ok(ReturnType { arrow, ty }))
    }
}

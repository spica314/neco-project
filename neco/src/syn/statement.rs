use super::{
    statement_expr::StatementExpr, statement_let::StatementLet, Parse, ParseError, ParseResult,
    Token,
};

#[derive(Debug, Clone)]
pub enum Statement {
    Let(StatementLet),
    Expr(StatementExpr),
}

impl Parse for Statement {
    type R = Statement;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        if let ParseResult::Ok(let_statement) = StatementLet::parse(tokens, i)? {
            eprintln!("let_statement = {:?}", let_statement);
            return Ok(ParseResult::Ok(let_statement));
        }

        if let ParseResult::Ok(expr_statement) = StatementExpr::parse(tokens, i)? {
            eprintln!("expr_statement = {:?}", expr_statement);
            return Ok(ParseResult::Ok(expr_statement));
        }

        eprintln!("fail (statement)");
        *i = initial_i;
        Ok(ParseResult::Fail)
    }
}

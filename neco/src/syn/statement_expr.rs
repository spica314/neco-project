use crate::syn::tokens::token_semi::TokenSemi;

use super::{expr::Expr, statement::Statement, Parse, ParseError, ParseResult, Token};

#[derive(Debug, Clone)]
pub struct StatementExpr {
    expr: Expr,
    semi: Token,
}

impl StatementExpr {
    pub fn expr(&self) -> &Expr {
        &self.expr
    }
    pub fn semi(&self) -> &Token {
        &self.semi
    }
}

impl Parse for StatementExpr {
    type R = Statement;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let expr = Expr::parse(tokens, i)?;
        if expr.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let expr = expr.unwrap();
        eprintln!("expr = {:?}", expr);

        let semi = TokenSemi::parse(tokens, i)?;
        if semi.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let semi = semi.unwrap();

        Ok(ParseResult::Ok(Statement::Expr(StatementExpr {
            expr,
            semi,
        })))
    }
}

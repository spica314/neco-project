use crate::syn::{
    parse_keyword,
    tokens::{token_assign::TokenAssign, token_ident::TokenIdent, token_semi::TokenSemi},
    ParseResult,
};

use super::{expr::Expr, statement::Statement, Parse, ParseError, Token};

#[derive(Debug, Clone)]
pub struct StatementLet {
    token_let: Token,
    token_mut: Option<Token>,
    ident: Token,
    assign: Token,
    expr: Expr,
    semi: Token,
}

impl StatementLet {
    pub fn token_let(&self) -> &Token {
        &self.token_let
    }
    pub fn token_mut(&self) -> &Option<Token> {
        &self.token_mut
    }
    pub fn ident(&self) -> &Token {
        &self.ident
    }
    pub fn assign(&self) -> &Token {
        &self.assign
    }
    pub fn expr(&self) -> &Expr {
        &self.expr
    }
    pub fn semi(&self) -> &Token {
        &self.semi
    }
}

impl Parse for StatementLet {
    type R = Statement;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }

        let initial_i = *i;

        let token_let = parse_keyword(tokens, i, "let")?;
        if token_let.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let token_let = token_let.unwrap();
        eprintln!("token_let = {:?}", token_let);

        let token_mut = parse_keyword(tokens, i, "mut")?;
        let token_mut = token_mut.to_option();
        eprintln!("token_mut = {:?}", token_mut);

        let ident = TokenIdent::parse(tokens, i)?;
        if ident.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let ident = ident.unwrap();
        eprintln!("ident = {:?}", ident);

        let assign = TokenAssign::parse(tokens, i)?;
        if assign.is_fail() {
            *i = initial_i;
            return Ok(ParseResult::Fail);
        }
        let assign = assign.unwrap();
        eprintln!("assign = {:?}", assign);

        let expr = Expr::parse(tokens, i)?;
        if expr.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let expr = expr.unwrap();
        eprintln!("expr = {:?}", expr);

        let semi = TokenSemi::parse(tokens, i)?;
        if semi.is_fail() {
            return Err(ParseError::Error(tokens[*i].begin_pos()));
        }
        let semi = semi.unwrap();
        eprintln!("semi = {:?}", semi);

        Ok(ParseResult::Ok(Statement::Let(StatementLet {
            token_let,
            token_mut,
            ident,
            assign,
            expr,
            semi,
        })))
    }
}

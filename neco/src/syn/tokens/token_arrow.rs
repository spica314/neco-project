use crate::syn::{Parse, ParseError, ParseResult, Token, TokenInner};

#[derive(Debug, Clone)]
pub struct TokenArrow;

impl Parse for TokenArrow {
    type R = Token;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }
        if let TokenInner::Arrow(_) = tokens[*i].inner() {
            let res = tokens[*i].clone();
            *i += 1;
            Ok(ParseResult::Ok(res))
        } else {
            Ok(ParseResult::Fail)
        }
    }
}

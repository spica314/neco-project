#[derive(Debug, Clone)]
pub struct TokenComment {
    comment: String,
}

impl TokenComment {
    pub fn comment(&self) -> &str {
        &self.comment
    }
}

use crate::syn::{Parse, ParseError, ParseResult, Token, TokenInner};

#[derive(Debug, Clone)]
pub struct TokenNumber {
    s: String,
}

impl TokenNumber {
    pub fn new(s: String) -> TokenNumber {
        TokenNumber { s }
    }
    pub fn as_str(&self) -> &str {
        &self.s
    }
}

impl Parse for TokenNumber {
    type R = Token;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }
        if let TokenInner::Number(_) = tokens[*i].inner() {
            let res = tokens[*i].clone();
            *i += 1;
            Ok(ParseResult::Ok(res))
        } else {
            Ok(ParseResult::Fail)
        }
    }
}

use crate::syn::{Parse, ParseError, ParseResult, Token, TokenInner};

#[derive(Debug, Clone)]
pub struct TokenPound;

impl Parse for TokenPound {
    type R = Token;

    fn parse(tokens: &[Token], i: &mut usize) -> Result<ParseResult<Self::R>, ParseError> {
        if *i >= tokens.len() {
            return Ok(ParseResult::Fail);
        }
        if let TokenInner::Pound(_) = tokens[*i].inner() {
            let res = tokens[*i].clone();
            *i += 1;
            Ok(ParseResult::Ok(res))
        } else {
            Ok(ParseResult::Fail)
        }
    }
}

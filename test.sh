#!/bin/bash

REPOSITORY_PATH=$(git rev-parse --show-toplevel)

cd $REPOSITORY_PATH

# cargo fmt
cargo fmt --all -- --check
if [ $? -ne 0 ]; then
    exit 1
fi

# cargo test
cargo test
if [ $? -ne 0 ]; then
    exit 1
fi

# other tests
cargo run -- ./felis_testcases/phase_1/phase_1_1.fe
if [ $? -ne 42 ]; then
    exit 1
fi

cargo run -- ./felis_testcases/phase_2/phase_2_1.fe
if [ $? -ne 42 ]; then
    exit 1
fi

cargo run -- ./felis_testcases/phase_3/phase_3_1.fe
if [ $? -ne 42 ]; then
    exit 1
fi

cargo run -- ./felis_testcases/phase_4/phase_4_1.fe
if [ $? -ne 0 ]; then
    exit 1
fi

cargo run -- --test ./felis_testcases/phase_4/phase_4_1.fe
if [ $? -ne 0 ]; then
    exit 1
fi

cargo run -- --test ./felis_testcases/phase_5/phase_5_1.fe
if [ $? -ne 0 ]; then
    exit 1
fi

cargo run -- --test ./felis_testcases/phase_6/phase_6_1.fe
if [ $? -ne 0 ]; then
    exit 1
fi

cargo run -- --test ./felis_testcases/phase_7/phase_7_1.fe
if [ $? -ne 0 ]; then
    exit 1
fi

cargo run -- --test ./felis_testcases/phase_7/phase_7_2.fe
if [ $? -ne 0 ]; then
    exit 1
fi

exit 0
